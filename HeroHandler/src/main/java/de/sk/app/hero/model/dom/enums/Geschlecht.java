package de.sk.app.hero.model.dom.enums;

public enum Geschlecht {
	
	MAENNLICH("maennlich"),
	
	WEIBLICH("weiblich");
	
	private String name;
	
	private Geschlecht(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

}
