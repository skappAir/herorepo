package de.sk.app.hero.model;

import java.util.Collections;
import java.util.List;

import de.sk.app.hero.model.dom.Basis;
import de.sk.app.hero.model.dom.Eigenschaft;
import de.sk.app.hero.model.dom.Ereignis;
import de.sk.app.hero.model.dom.Gegenstand;
import de.sk.app.hero.model.dom.Geldboerse;
import de.sk.app.hero.model.dom.Heldenausruestung;
import de.sk.app.hero.model.dom.Kampfwerte;
import de.sk.app.hero.model.dom.Mod;
import de.sk.app.hero.model.dom.Sonderfertigkeit;
import de.sk.app.hero.model.dom.Talent;
import de.sk.app.hero.model.dom.Verbindung;
import de.sk.app.hero.model.dom.Vorteil;
import de.sk.app.hero.model.dom.Zauber;

public class Hero {

	private List<Mod> mods;

	private Basis basis;

	private List<Eigenschaft> eigenschaften;

	private List<Vorteil> vt;

	private List<Sonderfertigkeit> sf;

	private List<Ereignis> ereignisse;

	private List<Talent> talentliste;

	private List<Zauber> zauberliste;

	private List<Kampfwerte> kampf;

	private List<Gegenstand> gegenstaende;

	private List<Heldenausruestung> ausruestungen;

	private List<Verbindung> verbindungen;

	private Geldboerse geldboerse;

	public Hero() {
		eigenschaften = Collections.emptyList();
		vt = Collections.emptyList();
		sf = Collections.emptyList();
		ereignisse = Collections.emptyList();
		talentliste = Collections.emptyList();
		zauberliste = Collections.emptyList();
		kampf = Collections.emptyList();
		gegenstaende = Collections.emptyList();
		ausruestungen = Collections.emptyList();
		verbindungen = Collections.emptyList();
	}

	public List<Mod> getMods() {
		return mods;
	}

	public void setMods(final List<Mod> mods) {
		this.mods = mods;
	}

	public Basis getBasis() {
		return basis;
	}

	public void setBasis(final Basis basis) {
		this.basis = basis;
	}

	public List<Eigenschaft> getEigenschaften() {
		return eigenschaften;
	}

	public void setEigenschaften(final List<Eigenschaft> eigenschaften) {
		this.eigenschaften = eigenschaften;
	}

	public List<Vorteil> getVt() {
		return vt;
	}

	public void setVt(final List<Vorteil> vt) {
		this.vt = vt;
	}

	public List<Sonderfertigkeit> getSf() {
		return sf;
	}

	public void setSf(final List<Sonderfertigkeit> sf) {
		this.sf = sf;
	}

	public List<Ereignis> getEreignisse() {
		return ereignisse;
	}

	public void setEreignisse(final List<Ereignis> ereignisse) {
		this.ereignisse = ereignisse;
	}

	public List<Talent> getTalentliste() {
		return talentliste;
	}

	public void setTalentliste(final List<Talent> talentliste) {
		this.talentliste = talentliste;
	}

	public List<Zauber> getZauberliste() {
		return zauberliste;
	}

	public void setZauberliste(final List<Zauber> zauberliste) {
		this.zauberliste = zauberliste;
	}

	public List<Kampfwerte> getKampf() {
		return kampf;
	}

	public void setKampf(final List<Kampfwerte> kampf) {
		this.kampf = kampf;
	}

	public List<Gegenstand> getGegenstaende() {
		return gegenstaende;
	}

	public void setGegenstaende(final List<Gegenstand> gegenstaende) {
		this.gegenstaende = gegenstaende;
	}

	public List<Heldenausruestung> getAusruestungen() {
		return ausruestungen;
	}

	public void setAusruestungen(final List<Heldenausruestung> ausruestungen) {
		this.ausruestungen = ausruestungen;
	}

	public List<Verbindung> getVerbindungen() {
		return verbindungen;
	}

	public void setVerbindungen(final List<Verbindung> verbindungen) {
		this.verbindungen = verbindungen;
	}

	public Geldboerse getGeldboerse() {
		return geldboerse;
	}

	public void setGeldboerse(final Geldboerse geldboerse) {
		this.geldboerse = geldboerse;
	}

}
