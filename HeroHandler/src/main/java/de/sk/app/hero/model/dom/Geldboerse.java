package de.sk.app.hero.model.dom;

public class Geldboerse {

	private int dukaten;

	private int silbertaler;

	private int heller;

	private int kreuzer;

	public int getDukaten() {
		return dukaten;
	}

	public void setDukaten(final int dukaten) {
		this.dukaten = dukaten;
	}

	public int getSilbertaler() {
		return silbertaler;
	}

	public void setSilbertaler(final int silbertaler) {
		this.silbertaler = silbertaler;
	}

	public int getHeller() {
		return heller;
	}

	public void setHeller(final int heller) {
		this.heller = heller;
	}

	public int getKreuzer() {
		return kreuzer;
	}

	public void setKreuzer(final int kreuzer) {
		this.kreuzer = kreuzer;
	}

}
