package de.sk.app.hero.view;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@SessionScoped
@Named("eigenschaftenView")
public class EigenschaftenView {

	private String mut;

	private String klugheit;

	private String intuition;

	private String charisma;

	private String fingerfertigkeit;

	private String gewandtheit;

	private String konstitution;

	private String koerperkraft;

	private String geschwindigkeit;

	public String getMut() {
		return mut;
	}

	public void setMut(final String mut) {
		this.mut = mut;
	}

	public String getKlugheit() {
		return klugheit;
	}

	public void setKlugheit(final String klugheit) {
		this.klugheit = klugheit;
	}

	public String getIntuition() {
		return intuition;
	}

	public void setIntuition(final String intuition) {
		this.intuition = intuition;
	}

	public String getCharisma() {
		return charisma;
	}

	public void setCharisma(final String charisma) {
		this.charisma = charisma;
	}

	public String getFingerfertigkeit() {
		return fingerfertigkeit;
	}

	public void setFingerfertigkeit(final String fingerfertigkeit) {
		this.fingerfertigkeit = fingerfertigkeit;
	}

	public String getGewandtheit() {
		return gewandtheit;
	}

	public void setGewandtheit(final String gewandtheit) {
		this.gewandtheit = gewandtheit;
	}

	public String getKonstitution() {
		return konstitution;
	}

	public void setKonstitution(final String konstitution) {
		this.konstitution = konstitution;
	}

	public String getKoerperkraft() {
		return koerperkraft;
	}

	public void setKoerperkraft(final String koerperkraft) {
		this.koerperkraft = koerperkraft;
	}

	public String getGeschwindigkeit() {
		return geschwindigkeit;
	}

	public void setGeschwindigkeit(final String geschwindigkeit) {
		this.geschwindigkeit = geschwindigkeit;
	}
}
