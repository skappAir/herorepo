package de.sk.app.hero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"de.sk.app.hero.*"})
public class HeroHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HeroHandlerApplication.class, args);
	}

}
