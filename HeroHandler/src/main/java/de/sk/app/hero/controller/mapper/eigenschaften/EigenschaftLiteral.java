package de.sk.app.hero.controller.mapper.eigenschaften;

import de.sk.app.hero.controller.mapper.MapperConstants;

public enum EigenschaftLiteral {

	MUT(MapperConstants.MUT, "MU"),

	KLUGHEIT(MapperConstants.KLUGHEIT, "KL"),

	INTUITION(MapperConstants.INTUITION, "IN"),

	CHARISMA(MapperConstants.CHARISMA, "CH"),

	FINGERFERTIGKEIT(MapperConstants.FINGERFERTIGKEIT, "FF"),

	GEWANDTHEIT(MapperConstants.GEWANDTHEIT, "GE"),

	KONSTITUTION(MapperConstants.KONSTITUTION, "KO"),

	KOERPERKRAFT(MapperConstants.KOERPERKRAFT, "KK"),

	GESCHWINDIGKEIT(MapperConstants.GESCHWINDIGKEIT, "");

	private String name;

	private String value;

	private EigenschaftLiteral(final String name, final String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public String getValue() {
		return value;
	}

}
