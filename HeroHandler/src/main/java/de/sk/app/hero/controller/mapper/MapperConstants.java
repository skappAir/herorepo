package de.sk.app.hero.controller.mapper;

public class MapperConstants {

	public static final String MUT = "Mut";

	public static final String KLUGHEIT = "Klugheit";

	public static final String INTUITION = "Intuition";

	public static final String CHARISMA = "Charisma";

	public static final String FINGERFERTIGKEIT = "Fingerfertigkeit";

	public static final String GEWANDTHEIT = "Gewandtheit";

	public static final String KONSTITUTION = "Konstitution";

	public static final String KOERPERKRAFT = "Körperkraft";

	public static final String GESCHWINDIGKEIT = "Geschwindigkeit";
}
