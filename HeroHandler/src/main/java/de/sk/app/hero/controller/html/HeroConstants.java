package de.sk.app.hero.controller.html;

public class HeroConstants {

	public static final String class_heldenname = "heldenname";

	public static final String class_heldendaten = "heldendaten";

	public static final String class_personendaten = "personendaten";

	public static final String class_beschreibung = "beschreibung";

	public static final String class_persoenliches = "persoenliches";

	public static final String class_umfeld = "umfeld";

	public static final String class_eigenschaftenBasis = "eigenschaftenBasiswerte";

	public static final String class_eigenschaftenGitter = "eigenschaften gitternetz";

	public static final String class_basiswerteGitter = "basiswerte gitternetz";

	public static final String class_abenteuerpunkte = "abenteuerpunkte";

	public static final String class_ap = "ap";

	public static final String class_vorteileNachteile = "vorNachteile";

	public static final String class_vorteile = "vorteile";

	public static final String class_sonder = "sonderfertigkeiten";

	public static final String class_talente = "talente";

	public static final String class_talentgruppeGitter = "talentgruppe gitternetz";

	public static final String class_nahwaffen = "nkwaffen";

	public static final String class_nahWaffenGitter = "nkwaffen gitternetz";

	public static final String class_fernWaffen = "fkwaffen";

	public static final String class_fernWaffenGitter = "fkwaffen gitternetz";

	public static final String class_inventar = "inventar";

	public static final String class_inventarGitter = "inventar gitternetz";

	public static final String tag_table = "table";

	public static final String tag_tr = "tr";

}
