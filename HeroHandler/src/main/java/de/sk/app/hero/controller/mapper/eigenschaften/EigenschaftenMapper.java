package de.sk.app.hero.controller.mapper.eigenschaften;

import java.util.Arrays;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.sk.app.hero.model.dom.Eigenschaft;
import de.sk.app.hero.view.EigenschaftenView;

@SessionScoped
public class EigenschaftenMapper {

	private static final Logger LOGGER = LoggerFactory.getLogger(EigenschaftenMapper.class);

	@Inject
	private EigenschaftenView eigenschaftenView;

	public void map(final List<Eigenschaft> eigenschaftenListe) {
		List<EigenschaftLiteral> literals = Arrays.asList(EigenschaftLiteral.values());
		literals.stream().forEach(el -> setEigenschaft(el, eigenschaftenListe));
	}

	private void setEigenschaft(final EigenschaftLiteral el, final List<Eigenschaft> eigenschaftenList) {

		if (eigenschaftenView == null) {
			FacesContext context = FacesContext.getCurrentInstance();
			eigenschaftenView = context.getApplication().evaluateExpressionGet(context, "#{eigenschaftenView}",
					EigenschaftenView.class);
		}

		LOGGER.error("EigenschaftenView: {}", eigenschaftenView);
		LOGGER.error("EigenschaftLiteral: {}", el);
		LOGGER.error("Eigenschaften: {}", eigenschaftenList);

		if (eigenschaftenView == null) {
		}

		switch (el) {
		case MUT:
			eigenschaftenView.setMut(getEigenschaftByLiteral(el, eigenschaftenList).getAktuell());
			break;
		case CHARISMA:
			eigenschaftenView.setCharisma(getEigenschaftByLiteral(el, eigenschaftenList).getAktuell());
			break;
		case FINGERFERTIGKEIT:
			eigenschaftenView.setFingerfertigkeit(getEigenschaftByLiteral(el, eigenschaftenList).getAktuell());
			break;
		case GESCHWINDIGKEIT:
			eigenschaftenView.setGeschwindigkeit(getEigenschaftByLiteral(el, eigenschaftenList).getAktuell());
			break;
		case GEWANDTHEIT:
			eigenschaftenView.setGewandtheit(getEigenschaftByLiteral(el, eigenschaftenList).getAktuell());
			break;
		case INTUITION:
			eigenschaftenView.setIntuition(getEigenschaftByLiteral(el, eigenschaftenList).getAktuell());
			break;
		case KLUGHEIT:
			eigenschaftenView.setKlugheit(getEigenschaftByLiteral(el, eigenschaftenList).getAktuell());
			break;
		case KOERPERKRAFT:
			eigenschaftenView.setKoerperkraft(getEigenschaftByLiteral(el, eigenschaftenList).getAktuell());
			break;
		case KONSTITUTION:
			eigenschaftenView.setKonstitution(getEigenschaftByLiteral(el, eigenschaftenList).getAktuell());
			break;
		default:
			break;
		}
	}

	private Eigenschaft getEigenschaftByLiteral(final EigenschaftLiteral el,
			final List<Eigenschaft> eigenschaftenListe) {
		return eigenschaftenListe.stream().filter(e -> e.getName().equals(el.getName())).findAny().get();
	}

}
