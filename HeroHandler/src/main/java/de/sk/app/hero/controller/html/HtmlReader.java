package de.sk.app.hero.controller.html;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import javax.annotation.ManagedBean;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@ManagedBean(value = "htmlReader")
public class HtmlReader {

	public static String defaultFile = "C:/Users/seppe/Documents/DSA/BastarodibanGilbadbanKhalil.html";

	private Element body;

	public void readHtmlBody(final String path) throws MalformedURLException, IOException {
		File html = new File(path);
		Document doc = Jsoup.parse(html, "UTF-8", "http://example.com/");
		body = doc.body();
	}

	public Elements getElementsByClass(final String className) {
		return body.getElementsByClass(className);
	}

}
