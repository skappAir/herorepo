package de.sk.app.hero.model.dom;

public class Eigenschaft {

	private String name;

	private String modifikator;

	private String startwert;

	private String aktuell;

	private String zugekauft;

	private String max;

	private boolean basis = false;

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getModifikator() {
		return modifikator;
	}

	public int getModifikatorInteger() {
		return Integer.valueOf(modifikator);
	}

	public void setModifikator(final String modifikator) {
		this.modifikator = modifikator;
	}

	public String getStartwert() {
		return startwert;
	}

	public int getStartwertInteger() {
		return Integer.valueOf(startwert);
	}

	public void setStartwert(final String startwert) {
		this.startwert = startwert;
	}

	public int getAktuellInteger() {
		return Integer.valueOf(aktuell);
	}

	public String getAktuell() {
		return aktuell;
	}

	public void setAktuell(final String aktuell) {
		this.aktuell = aktuell;
	}

	public String getZugekauft() {
		return zugekauft;
	}

	public void setZugekauft(final String zugekauft) {
		this.zugekauft = zugekauft;
	}

	public String getMax() {
		return max;
	}

	public void setMax(final String max) {
		this.max = max;
	}

	public boolean isBasis() {
		return basis;
	}

	public void setBasis(final boolean basis) {
		this.basis = basis;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Eigenschaft [name=").append(name).append(", modifikator=").append(modifikator)
				.append(", startwert=").append(startwert).append(", aktuell=").append(aktuell).append(", zugekauft=")
				.append(zugekauft).append(", max=").append(max).append(", basis=").append(basis).append("]");
		return builder.toString();
	}

}
