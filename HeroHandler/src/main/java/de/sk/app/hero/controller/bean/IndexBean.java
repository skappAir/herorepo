package de.sk.app.hero.controller.bean;

import java.io.IOException;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.sk.app.hero.controller.html.HeroReader;
import de.sk.app.hero.controller.mapper.eigenschaften.EigenschaftenMapper;
import de.sk.app.hero.model.Hero;

@SessionScoped
@Named
public class IndexBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(IndexBean.class);

	private Hero hero;

	public void onload() {
		HeroReader heroReader = new HeroReader();

		try {
			heroReader.read();
			hero = heroReader.getHero();
			EigenschaftenMapper mapper = new EigenschaftenMapper();
			LOGGER.debug("hero.eigenschaften {}", hero.getEigenschaften());
			mapper.map(hero.getEigenschaften());
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public Hero hero() {
		return hero;
	}

}
