package de.sk.app.hero;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import de.sk.app.hero.view.EigenschaftenView;

@Configuration
public class HeroConfiguration {

	@Bean
	public EigenschaftenView getEigenschaftenView() {
		return new EigenschaftenView();
	}
}
