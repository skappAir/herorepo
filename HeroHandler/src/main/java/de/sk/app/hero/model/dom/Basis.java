package de.sk.app.hero.model.dom;

import java.util.List;

public class Basis {

	private String geschlecht;

	private List<Inc> settings;

	private Rasse rasse;

	private Kultur kultur;

	private List<Ausbildung> ausbildungen;

	private String verify;

	private String[] notiz = new String[10];

	private String abenteuerpunkte;

	private String freieabenteuerpunkte;

	public String getGeschlecht() {
		return geschlecht;
	}

	public void setGeschlecht(final String geschlecht) {
		this.geschlecht = geschlecht;
	}

	public List<Inc> getSettings() {
		return settings;
	}

	public void setSettings(final List<Inc> settings) {
		this.settings = settings;
	}

	public Rasse getRasse() {
		return rasse;
	}

	public void setRasse(final Rasse rasse) {
		this.rasse = rasse;
	}

	public Kultur getKultur() {
		return kultur;
	}

	public void setKultur(final Kultur kultur) {
		this.kultur = kultur;
	}

	public List<Ausbildung> getAusbildungen() {
		return ausbildungen;
	}

	public void setAusbildungen(final List<Ausbildung> ausbildungen) {
		this.ausbildungen = ausbildungen;
	}

	public String getVerify() {
		return verify;
	}

	public void setVerify(final String verify) {
		this.verify = verify;
	}

	public String[] getNotiz() {
		return notiz;
	}

	public void setNotiz(final String[] notiz) {
		this.notiz = notiz;
	}

	public String getAbenteuerpunkte() {
		return abenteuerpunkte;
	}

	public void setAbenteuerpunkte(final String abenteuerpunkte) {
		this.abenteuerpunkte = abenteuerpunkte;
	}

	public String getFreieabenteuerpunkte() {
		return freieabenteuerpunkte;
	}

	public void setFreieabenteuerpunkte(final String freieabenteuerpunkte) {
		this.freieabenteuerpunkte = freieabenteuerpunkte;
	}

}
