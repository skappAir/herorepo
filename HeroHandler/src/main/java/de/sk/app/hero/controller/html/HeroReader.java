package de.sk.app.hero.controller.html;

import static de.sk.app.hero.controller.html.HeroConstants.class_basiswerteGitter;
import static de.sk.app.hero.controller.html.HeroConstants.class_eigenschaftenGitter;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.sk.app.hero.model.Hero;
import de.sk.app.hero.model.dom.Eigenschaft;

public class HeroReader {

	private static final Logger LOGGER = LoggerFactory.getLogger(HeroReader.class);

	private final HtmlReader htmlReader = new HtmlReader();

	private Hero hero;

	public void read() throws MalformedURLException, IOException {
		htmlReader.readHtmlBody(HtmlReader.defaultFile);
		hero = new Hero();
		readEigenschaften(false);
	}

	private void readEigenschaften(final boolean basis) {
		Elements elements = htmlReader.getElementsByClass(basis ? class_basiswerteGitter : class_eigenschaftenGitter);

		if (elements != null && elements.toArray().length > 0) {
			Elements trElements = elements.get(0).getElementsByTag("tr");
			LOGGER.error("{}", trElements);

			if (trElements != null && trElements.toArray().length > 0) {
				List<Eigenschaft> list = new ArrayList<>();
				trElements.stream().forEach(el -> {
					list.add(readEigenschaft(el, basis));
				});
				hero.setEigenschaften(list);
			}
		}
	}

	private

	private Eigenschaft readEigenschaft(final Element el, final boolean basis) {
		Eigenschaft e = new Eigenschaft();
		e.setName(el.getElementsByClass("name").get(0).text());
		e.setModifikator(el.getElementsByClass("modifikator").get(0).text());
		e.setStartwert(el.getElementsByClass("start").get(0).text());
		e.setAktuell(el.getElementsByClass("aktuell").get(0).text());

		if (basis) {
			e.setBasis(basis);
			e.setZugekauft(el.getElementsByClass("zugekauft").get(0).text());
			e.setMax(el.getElementsByClass("max").get(0).text());
		}
		LOGGER.error("Eigenschaft: ", e);
		return e;
	}

	public Hero getHero() {
		return hero;
	}

}
